package nfree_mgouletas.svu_location;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class EditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main_edit);

        Spinner dropdown = (Spinner)findViewById(R.id.spinner_building_type);
        String[] items = new String[]{"House", "Apartment", "Trailer", "Other"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //String selected_item = dropdown.getSelectedItem().toString();
                EditText apartmentnumber = (EditText)findViewById(R.id.apartment_number);
                EditText trailernumber = (EditText)findViewById(R.id.lot_number);
                switch (i) {
                    case 1:
                        apartmentnumber.setVisibility(View.VISIBLE);
                        trailernumber.setVisibility(View.GONE);
                        break;
                    case 2:
                        trailernumber.setVisibility(View.VISIBLE);
                        apartmentnumber.setVisibility(View.GONE);
                        break;
                    default:
                        apartmentnumber.setVisibility(View.GONE);
                        trailernumber.setVisibility(View.GONE);
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        Button button_finish = (Button) findViewById(R.id.button_finish);
        button_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK, getIntent());
                finish();
            }
        });

    }
}
